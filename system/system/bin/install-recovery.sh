#!/system/bin/sh
if ! applypatch --check EMMC:/dev/block/platform/bootdevice/by-name/recovery:33554432:803ee669f3ee90c8f9cda9c64a3db80c0940bf23; then
  applypatch  \
          --patch /system/recovery-from-boot.p \
          --source EMMC:/dev/block/platform/bootdevice/by-name/boot:33554432:2cf02f203d8cc41ee389a94c22031c9a97ba8a6a \
          --target EMMC:/dev/block/platform/bootdevice/by-name/recovery:33554432:803ee669f3ee90c8f9cda9c64a3db80c0940bf23 && \
      log -t recovery "Installing new recovery image: succeeded" || \
      log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
