#
# Copyright (C) 2022 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_TECNO-KE7.mk

COMMON_LUNCH_CHOICES := \
    lineage_TECNO-KE7-user \
    lineage_TECNO-KE7-userdebug \
    lineage_TECNO-KE7-eng
