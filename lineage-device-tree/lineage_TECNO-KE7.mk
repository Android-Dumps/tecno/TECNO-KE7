#
# Copyright (C) 2022 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from TECNO-KE7 device
$(call inherit-product, device/tecno/TECNO-KE7/device.mk)

PRODUCT_DEVICE := TECNO-KE7
PRODUCT_NAME := lineage_TECNO-KE7
PRODUCT_BRAND := TECNO
PRODUCT_MODEL := TECNO KE7
PRODUCT_MANUFACTURER := tecno

PRODUCT_GMS_CLIENTID_BASE := android-transsion-tecno-rev1

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="sys_tssi_64_tecno-user 10 QP1A.190711.020 21500 release-keys"

BUILD_FINGERPRINT := TECNO/KE7-GL/TECNO-KE7:10/QP1A.190711.020/GHIJKM-GL-200617V060:user/release-keys
